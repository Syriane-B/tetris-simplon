import IShape from "../models/IShape";
import JShape from "../models/JShape";
import LShape from "../models/LShape";
import OShape from "../models/OShape";
import SShape from "../models/SShape";
import TShape from "../models/TShape";
import ZShape from "../models/ZShape";

export const gameName: string = 'TETRIS';
export const gridWidth: number = 10;
export const gridHeight: number = 20;
export const blocSize: number = 30;
export const $screen = document.getElementById('screen');
export const blocColors: Array<string> = [
    '0',
    '#BF5349',
    '#8C3503',
    '#F2DA91',
    '#F2B807',
    '#1BDDF2',
    '#8C2066',
    '#F29472'
]
export function randomIntFromInterval(min: number, max: number) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
}
export const keysDirection: Array<string> = ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];
export const blocGeneratorSpeed: number[] = [
    1000,
    750,
    500
];
export const differentShapes: Array<any> = [
    IShape,
    JShape,
    LShape,
    OShape,
    SShape,
    TShape,
    ZShape
];
export const scoreForFullLine: number[] = [
    200,
    350,
    550
];
export const scoreForGameOver: number[] = [
    -400,
    -500,
    -600
];
