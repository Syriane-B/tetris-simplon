export default class Score {
    public value: number;
    public scoreDomWindows: HTMLElement;
    public scoreDomValue: HTMLElement;
    constructor() {
        // get the score in the browser
        if(localStorage.getItem("TetrisScore") !== null) {
            this.value = parseInt(localStorage.getItem("TetrisScore"));
        } else {
            this.value = 0;
        }
        this.scoreDomWindows = document.createElement('div');
        this.scoreDomWindows.className = 'scoreBloc';
        this.scoreDomWindows.id = 'scoreBloc';
        let scoreTitle = document.createElement('h4');
        scoreTitle.innerText = 'Score';
        this.scoreDomWindows.appendChild(scoreTitle);
        this.scoreDomValue = document.createElement('p');
        this.scoreDomValue.innerText = this.value.toString();
        this.scoreDomWindows.appendChild(this.scoreDomValue);
    }
    getScore() {
        return this.value;
    }
    setScore(n: number) {
        this.value = this.value + n;
        this.saveScoreInBrowser();
        this.updateDomScore();
        return this.value;
    }
    saveScoreInBrowser() {
        // save the score in the navigator
        localStorage.setItem("TetrisScore", this.value.toString());
    }
    updateDomScore() {
        this.scoreDomValue.innerText = this.value.toString();
    }
}
