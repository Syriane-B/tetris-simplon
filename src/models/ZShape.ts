import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class ZShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let zShape = document.createElement('div');
        zShape.style.backgroundColor = blocColors[this.color];
        zShape.style.left = '0px';
        zShape.style.top = blocSize+'px';
        zShape.style.width = 2*blocSize+'px';
        zShape.style.height = blocSize+'px';
        this.domBloc.appendChild(zShape);
        let zShape2 = document.createElement('div');
        zShape2.style.backgroundColor = blocColors[this.color];
        zShape2.style.left = blocSize+'px';
        zShape2.style.top = 2*blocSize+'px';
        zShape2.style.width = 2*blocSize+'px';
        zShape2.style.height = blocSize+'px';
        this.domBloc.appendChild(zShape2);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,0,0,0],
                    [1,1,0,0],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
            case 1:
                return [
                    [0,0,1,0],
                    [0,1,1,0],
                    [0,1,0,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [0,0,1,1],
                    [0,0,0,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,0,1,0],
                    [0,1,1,0],
                    [0,1,0,0],
                ];
        }
    }
}
