import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class IShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let iShape = document.createElement('div');
        iShape.style.backgroundColor = blocColors[this.color];
        iShape.style.left = blocSize+'px';
        iShape.style.top = '0px';
        iShape.style.width = blocSize+'px';
        iShape.style.height = 4*blocSize+'px';
        this.domBloc.appendChild(iShape);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,1,0,0],
                    [0,1,0,0],
                    [0,1,0,0],
                    [0,1,0,0],
                ];
            case 1:
                return [
                    [0,0,0,0],
                    [1,1,1,1],
                    [0,0,0,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,1,0],
                    [0,0,1,0],
                    [0,0,1,0],
                    [0,0,1,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,0,0,0],
                    [1,1,1,1],
                    [0,0,0,0],
                ];
        }
    }
}
