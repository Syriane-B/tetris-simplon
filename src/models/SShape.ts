import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class SShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let sShape = document.createElement('div');
        sShape.style.backgroundColor = blocColors[this.color];
        sShape.style.left = blocSize+'px';
        sShape.style.top = blocSize+'px';
        sShape.style.width = 2*blocSize+'px';
        sShape.style.height = blocSize+'px';
        this.domBloc.appendChild(sShape);
        let sShape2 = document.createElement('div');
        sShape2.style.backgroundColor = blocColors[this.color];
        sShape2.style.left = '0px';
        sShape2.style.top = 2*blocSize+'px';
        sShape2.style.width = 2*blocSize+'px';
        sShape2.style.height = blocSize+'px';
        this.domBloc.appendChild(sShape2);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [1,1,0,0],
                    [0,0,0,0],
                ];
            case 1:
                return [
                    [0,1,0,0],
                    [0,1,1,0],
                    [0,0,1,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,0,0],
                    [0,0,1,1],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,1,0,0],
                    [0,1,1,0],
                    [0,0,1,0],
                ];
        }
    }
}
