import Grid from "./Grid";
import {
    blocSize,
    gridWidth,
    keysDirection,
    randomIntFromInterval,
    blocColors,
    scoreForGameOver
} from "../config/constant";
import {gameOver} from "../views/gameover";

export default class Bloc {
    public isActive: boolean;
    public interval: any;
    public color: number;
    public grid: Grid;
    public posX: number;
    public posY: number;
    public domBloc: HTMLElement;
    public rotation: number;
    constructor(grid: Grid) {
        this.rotation = 0;
        this.isActive = true;
        this.color = randomIntFromInterval(1, blocColors.length-1);
        this.interval = setInterval(() => {
            this.fall();
        }, grid.currentBlocSpeed);
        this.posX = Math.round(gridWidth/2)-2;
        this.posY = -1;
        this.domBloc = document.createElement('div');
        this.domBloc.className = 'bloc';
        this.domBloc.style.width = 4*blocSize+'px';
        this.domBloc.style.height = 4*blocSize+'px';
        this.domBloc.style.left = this.posX*blocSize+'px';
        this.domBloc.style.top = this.posY*blocSize+'px';
        this.grid = grid;
    }
    fall() {
        if(this.grid.checkIfBlocCanGoToThisPosition(this.getShapeGrid(), this.posX, this.posY+1)) {
            this.posY = this.posY +1;
            this.domBloc.style.top = this.posY*blocSize+'px';
            return true;
        } else if (this.posY == -1) {
            clearInterval(this.interval);
            this.grid.score.setScore(scoreForGameOver[this.grid.levelSelected]);
            gameOver();
            this.isActive = false;
        } else {
            this.stop();
        }
    }
    stop() {
        // stop the bloc fall
        clearInterval(this.interval);
        // print the bloc in the grid
        this.grid.handleBlocStop();
        // set isActive to false
        this.isActive = false;
    }
    handleMove(key: string): boolean {
        if (this.isActive) {
            switch (keysDirection.indexOf(key)) {
                case 0:
                    return this.rotate();
                case 1:
                    return this.fall();
                case 2:
                    return this.moveLeft();
                case 3:
                    return this.moveRight();
                default:
                    return false;
            }
        }
    }
    rotate(): boolean {
        if(this.grid.checkIfBlocCanGoToThisPosition(this.getShapeGrid((this.rotation+1)%4), this.posX, Math.max(0, this.posY))) {
            this.rotation = (this.rotation + 1) % 4;
            this.domBloc.style.transform = 'rotate(' + (this.rotation * 90) + 'deg)';
            return true;
        } else {
            return false;
        }
    }
    moveRight(): boolean {
        if(this.grid.checkIfBlocCanGoToThisPosition(this.getShapeGrid(), this.posX+1, Math.max(0, this.posY))) {
            this.posX = this.posX +1;
            this.domBloc.style.left = this.posX*blocSize+'px';
            return true;
        } else {
            return false;
        }
    }
    moveLeft(): boolean {
        if(
            this.grid.checkIfBlocCanGoToThisPosition(this.getShapeGrid(), this.posX-1, Math.max(0, this.posY))
        ) {
            this.posX = this.posX - 1;
            this.domBloc.style.left = this.posX * blocSize + 'px';
            return true;
        } else {
            return false;
        }
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        // this function is implemented in all shape that extends this bloc
        return [[0]];
    }
}
