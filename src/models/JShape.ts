import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class JShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let jShape = document.createElement('div');
        jShape.style.backgroundColor = blocColors[this.color];
        jShape.style.left = '0px';
        jShape.style.top = 2*blocSize+'px';
        jShape.style.width = blocSize+'px';
        jShape.style.height = blocSize+'px';
        this.domBloc.appendChild(jShape);
        let jShape2 = document.createElement('div');
        jShape2.style.backgroundColor = blocColors[this.color];
        jShape2.style.left = blocSize+'px';
        jShape2.style.top = '0px';
        jShape2.style.width = blocSize+'px';
        jShape2.style.height = 3*blocSize+'px';
        this.domBloc.appendChild(jShape2);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,1,0,0],
                    [0,1,0,0],
                    [1,1,0,0],
                    [0,0,0,0],
                ];
            case 1:
                return [
                    [0,1,0,0],
                    [0,1,1,1],
                    [0,0,0,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,0,0],
                    [0,0,1,1],
                    [0,0,1,0],
                    [0,0,1,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,0,0,0],
                    [1,1,1,0],
                    [0,0,1,0],
                ];
        }
    }
}
