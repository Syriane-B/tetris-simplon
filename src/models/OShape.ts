import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class OShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let oShape = document.createElement('div');
        oShape.style.backgroundColor = blocColors[this.color];
        oShape.style.left = blocSize+'px';
        oShape.style.top = blocSize+'px';
        oShape.style.width = 2*blocSize+'px';
        oShape.style.height = 2*blocSize+'px';
        this.domBloc.appendChild(oShape);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
            case 1:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
        }
    }
}
