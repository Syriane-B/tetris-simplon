import {
    gridWidth,
    gridHeight,
    blocSize,
    $screen,
    differentShapes,
    blocColors,
    randomIntFromInterval,
    blocGeneratorSpeed,
    scoreForFullLine
} from "../config/constant";
import Bloc from "./Bloc";
import Score from "./Score";

export default class Grid {
    public occupiedGrid: Array<number[]>;
    public domGrid: HTMLElement;
    public currentActiveBloc: Bloc;
    public currentBlocSpeed: number;
    public score: Score;
    public levelSelected;
    constructor(levelSelected: number) {
        this.levelSelected = levelSelected;
        this.currentBlocSpeed = blocGeneratorSpeed[this.levelSelected];
        this.occupiedGrid = [];
        // Creates all lines:
        //for first level
        console.log(levelSelected);
        switch (levelSelected) {
            case 0:
                for (let i = 0; i < gridHeight; i++) {
                    // Creates an empty line
                    this.occupiedGrid.push([]);
                    // Adds cols to the empty line:
                    for (let j = 0; j < gridWidth; j++) {
                        // Initializes:
                        this.occupiedGrid[i][j] = 0;
                    }
                }
                break;
            case 1:
                this.occupiedGrid = [
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [2, 2, 0, 4, 4, 0, 0, 0, 0, 0],
                    [2, 4, 0, 0, 4, 0, 0, 3, 3, 3],
                    [2, 4, 4, 4, 4, 1, 0, 1, 1, 0],
                ];
                break;
            case 2:
                this.occupiedGrid = [
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 2, 4, 4, 4, 0, 0, 0, 0, 0],
                    [0, 2, 0, 0, 0, 0, 3, 3, 3, 3],
                    [5, 5, 5, 5, 5, 1, 0, 1, 1, 0],
                ];
                break;
            default :
                for (let i = 0; i < gridHeight; i++) {
                    // Creates an empty line
                    this.occupiedGrid.push([]);
                    // Adds cols to the empty line:
                    for (let j = 0; j < gridWidth; j++) {
                        // Initializes:
                        this.occupiedGrid[i][j] = 0;
                    }
                }
                break;
        }
        this.score = new Score();
        this.domGrid = document.createElement('div');
        this.domGrid.id = 'dom-grid';
        this.domGrid.className = 'dom-grid';
        this.domGrid.style.width = gridWidth * blocSize + 'px';
        this.domGrid.style.height = gridHeight * blocSize + 'px';
        $screen.appendChild(this.score.scoreDomWindows);
        $screen.appendChild(this.domGrid);
        this.renderOccupiedlocGrid();
        this.generateBloc();
    }
    handleBlocStop(): void{
        // print the block
        this.printBloc(this.currentActiveBloc);
        // render the grid blocks & remove domBloc Element
        this.currentActiveBloc.domBloc.remove();
        this.renderOccupiedlocGrid();
        //
        this.checkFullLine();
        //Create a new bloc !
        this.currentActiveBloc = this.generateBloc();
    }
    generateBloc(): Bloc {
        if(this.currentBlocSpeed > 200) {
            this.currentBlocSpeed = this.currentBlocSpeed * 0.98;
        }
        let index = randomIntFromInterval(0, differentShapes.length-1);
        let ShapeClass = differentShapes[index];
        this.currentActiveBloc = new ShapeClass(this);
        return this.currentActiveBloc;
    }
    checkIfBlocCanGoToThisPosition(shapeGrid: Array<number[]>, newPosX: number, newPosY: number): boolean {
        let canBlocGoThere = true;
        //for each line of bloc
        shapeGrid.forEach((line, blocY) => {
            //foreach box
            line.forEach((box, blocX) => {
                // if the box is full check in the grid
                if (box === 1) {
                    //if the grid contain a bloc here
                    // or newPosX is outside the grid
                    // or newPosY is outside the grid
                    // set the result to false
                    if(
                        newPosX+blocX < 0 ||
                        (newPosX+blocX) >= (gridWidth) ||
                        (newPosY+blocY) >= (gridHeight) ||
                        this.occupiedGrid[newPosY+blocY][newPosX+blocX] !== 0
                    ) {
                        canBlocGoThere = false;
                    }
                }
            });
        })
        return canBlocGoThere;
    }
    printBloc(bloc: Bloc) {
        // save the bloc position to the grid
        bloc.getShapeGrid().forEach((line, blocY) => {
            //foreach box
            line.forEach((box, blocX) => {
                // if the box is full check in the grid
                if (box === 1) {
                    this.occupiedGrid[bloc.posY+blocY][bloc.posX+blocX] = bloc.color;
                }
            });
        })
    }
    renderOccupiedlocGrid() {
        this.domGrid.innerHTML = '';
        this.occupiedGrid.forEach((line: number[], gridY: number) => {
            line.forEach((col: number, gridX: number) => {
                if (col !== 0) {
                    const newDomBloc = document.createElement('div');
                    newDomBloc.className = 'occupiedGridBloc';
                    newDomBloc.style.backgroundColor = blocColors[col];
                    newDomBloc.style.width = blocSize + 'px';
                    newDomBloc.style.height = blocSize + 'px';
                    newDomBloc.style.top = gridY*blocSize+'px';
                    newDomBloc.style.left = gridX*blocSize+'px';
                    this.domGrid.appendChild(newDomBloc);
                }
            });
        });
    }
    checkFullLine() {
        let indexOfFullLine: number[] = [];
        this.occupiedGrid.forEach((line: number[], gridY: number) => {
            let isLineFull: boolean = true;
            line.forEach((col: number) => {
                if (col === 0) {
                    isLineFull = false;
                }
            });
            // if no empty col have been found, the line is considered as full
            if (isLineFull === true) {
                indexOfFullLine.push(gridY);
            }
        });
        // remove each full line from the array, push a new one empty to the top and clean line indexes
        if (indexOfFullLine.length !== 0) {
            indexOfFullLine.forEach(lineIndex => {
                this.deleteFullLine(lineIndex);
                this.score.setScore(scoreForFullLine[this.levelSelected]);
            })
        }
        this.renderOccupiedlocGrid();
    }
    deleteFullLine(lineIndex: number) {
        this.occupiedGrid.splice(lineIndex, 1);
        let newEmptyLine: number[] = [];
        for (let i = 0; i < gridWidth; i++) {
            // Initializes:
            newEmptyLine[i] = 0;
        }
        this.occupiedGrid.unshift(newEmptyLine);
    }
}
