import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class TShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let tShape = document.createElement('div');
        tShape.style.backgroundColor = blocColors[this.color];
        tShape.style.left = '0px';
        tShape.style.top = blocSize+'px';
        tShape.style.width = 3*blocSize+'px';
        tShape.style.height = blocSize+'px';
        this.domBloc.appendChild(tShape);
        let tShape2 = document.createElement('div');
        tShape2.style.backgroundColor = blocColors[this.color];
        tShape2.style.left = blocSize+'px';
        tShape2.style.top = 2*blocSize+'px';
        tShape2.style.width = blocSize+'px';
        tShape2.style.height = blocSize+'px';
        this.domBloc.appendChild(tShape2);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,0,0,0],
                    [1,1,1,0],
                    [0,1,0,0],
                    [0,0,0,0],
                ];
            case 1:
                return [
                    [0,0,1,0],
                    [0,1,1,0],
                    [0,0,1,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,0,0],
                    [0,0,1,0],
                    [0,1,1,1],
                    [0,0,0,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,1,0,0],
                    [0,1,1,0],
                    [0,1,0,0],
                ];
        }
    }
}
