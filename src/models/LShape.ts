import Bloc from './Bloc';
import {blocColors, blocSize} from "../config/constant";
import Grid from "./Grid";

export default class LShape extends Bloc {

    constructor(grid: Grid) {
        super(grid);
        let lShape = document.createElement('div');
        lShape.style.backgroundColor = blocColors[this.color];
        lShape.style.left = blocSize+'px';
        lShape.style.top = '0px';
        lShape.style.width = blocSize+'px';
        lShape.style.height = 3*blocSize+'px';
        this.domBloc.appendChild(lShape);
        let lShape2 = document.createElement('div');
        lShape2.style.backgroundColor = blocColors[this.color];
        lShape2.style.left = 2*blocSize+'px';
        lShape2.style.top = 2*blocSize+'px';
        lShape2.style.width = blocSize+'px';
        lShape2.style.height = blocSize+'px';
        this.domBloc.appendChild(lShape2);
        grid.domGrid.appendChild(this.domBloc);
    }
    getShapeGrid(rotation: number = this.rotation): Array<number[]> {
        switch(rotation) {
            case 0:
                return [
                    [0,1,0,0],
                    [0,1,0,0],
                    [0,1,1,0],
                    [0,0,0,0],
                ];
            case 1:
                return [
                    [0,0,0,0],
                    [0,1,1,1],
                    [0,1,0,0],
                    [0,0,0,0],
                ];
            case 2:
                return [
                    [0,0,0,0],
                    [0,1,1,0],
                    [0,0,1,0],
                    [0,0,1,0],
                ];
            case 3:
                return [
                    [0,0,0,0],
                    [0,0,1,0],
                    [1,1,1,0],
                    [0,0,0,0],
                ];
        }
    }
}
