import {initGame} from "./game";
import {gameName} from "../config/constant";
import Score from "../models/Score";

export function startGame() {
    // generate the level selector card card + title + select + button
    const startCard = document.createElement('div');
    startCard.className = "startCard";
    const title = document.createElement('h3');
    title.innerText = gameName;
    startCard.appendChild(title);
    let score = new Score();
    const scoreDom = document.createElement('p');
    scoreDom.innerText = 'Score précédent : ' + score.getScore().toString();
    startCard.appendChild(scoreDom);
    const startBtn = document.createElement('button');

    //select level
    const levelSelection = document.createElement('h5');
    levelSelection.innerText = "Please choose a level";
    levelSelection.style.color = "white";
    startCard.appendChild(levelSelection);
    const select = document.createElement('select');
    select.id = "selectLevel";
    startCard.appendChild(select);
    const level1 = document.createElement('option');
    level1.value = "0";
    level1.innerText = "easy";
    select.appendChild(level1);
    const level2 = document.createElement('option');
    level2.value = "1";
    level2.innerText = "medium";
    select.appendChild(level2);
    const level3 = document.createElement('option');
    level3.value = "2";
    level3.innerText = "hard";
    select.appendChild(level3);

    startBtn.innerText = "Start !";
    startBtn.addEventListener('click', () => {
        // remove the card on boarding card
        startCard.remove();
        initGame(parseInt(select.value));
    });
    startCard.appendChild(startBtn);

    const screen = document.getElementById('screen');
    screen.appendChild(startCard);


}


