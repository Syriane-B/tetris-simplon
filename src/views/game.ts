import Grid from "../models/Grid";
import {keysDirection} from "../config/constant";

export function initGame(levelSelected: number) {
    // create the grid
    let grid = new Grid(levelSelected);
    // link the keyboad event to the current active bloc of the grid
    document.addEventListener('keydown', function(event) {
        if (keysDirection.includes(event.key)){
            grid.currentActiveBloc.handleMove(event.key);
        }
    });
}
