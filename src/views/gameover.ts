import Score from "../models/Score";

export function gameOver() {
    // generate the level selector card card + title + select + button
    const gameOverCard = document.createElement('div');
    gameOverCard.className = "gameOverCard";
    const title = document.createElement('h3');
    title.innerText = 'Game Over';
    gameOverCard.appendChild(title);
    let score = new Score();
    const scoreDom = document.createElement('p');
    scoreDom.innerText = 'Nouveau score : ' + score.getScore().toString();
    gameOverCard.appendChild(scoreDom);
    const restartBtn = document.createElement('button');
    restartBtn.innerText = "Restart new game !";
    restartBtn.addEventListener('click', () => {
        // remove the card on boarding card
        document.location.reload();
    });
    gameOverCard.appendChild(restartBtn);
    const screen = document.getElementById('screen');
    screen.appendChild(gameOverCard);
}
